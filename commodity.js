//Constructor for commidity class.
function Commodity(name, info, code){
    this.name = name;
    this.info = info;
    this.code = code;
}
//Initialize variables and arrays used.
var commodities = [];
var widgets = [];

var dataSetArray = [];
var myChart;
var chartMulti;
var fullData;

var tempInfo;
var tempName;
var tempSymbol;
var active;

//Fetches information from commodities database, calls selectBox function.
function getCommodities(){

    fetch('getCommodities.php')
    .then(response => response.json())
    .then(selectBox)
    .catch(error => console.error(error));

}

//Gets response from get commities and puts commities into drop down box.
function selectBox(options){
    const itemSelect = document.getElementById("select-item");

    //Makes commities from response array into Commodity objects.
    for(let i = 0; i < options.length; i++){
        let c = new Commodity(options[i].name, options[i].information, options[i].code);
    //Pushes new commodity object into array.
        commodities.push(c);
    }

    commodities.sort(cSort);
    //Gets each commotidy name from array and makes a drop down box.
    for(let j=0; j<commodities.length; j++){
        const option = document.createElement("option");
        option.setAttribute("id", commodities[j].name);
        option.text = commodities[j].name;
        option.value = commodities[j].name;
        itemSelect.appendChild(option);

    }

    //Event listener for when commodity is selected from drop down box.
    itemSelect.addEventListener("change", function(){

         const selectedItem = itemSelect.selectedOptions[0];
         const selectedItemName = selectedItem.text;

         if(widgets.includes(selectedItemName)){
            return;
         }

         widgets.push(selectedItemName);
        
         //Finds commodity symbol to be used in api request.
         let b = commodities.find(commodity => commodity.name ===selectedItemName);
         tempName = b.name;
         tempInfo = b.info;
         var symbol = b.code;
         window.tempSymbol = symbol;
        //Fetches info using API request.
        fetch('https://www.alphavantage.co/query?function='+symbol+'&interval=annual&apikey=AFCA81U3ZLE4XVXE')
        .then(response => response.json())
        //Gets nessary data from api response.
        .then(showData => {
            var symbol = window.tempSymbol;

            fullData = showData.data;
            var data;
            var label;

            var labelArray = [];
            var dataArray = [];
            //Gets all information from response nessesary to contruct a graph.
            for(var i = 0; i < fullData.length; i++){
        
                label = fullData[i].date;
                data = fullData[i].value;
                labelArray.push(label);
                dataArray.push(data);
        
            }
            console.log(labelArray);
            console.log(dataArray);
            //Makes a new dataset to be used when creating graphs.
            let d = new DataSet(symbol, labelArray, dataArray);
            dataSetArray.push(d);
            console.log(dataSetArray.length);

        })
        //Makes a widget of selected commodity.
        .then(makeWidget => {
            var container = document.createElement("div");
            container.setAttribute("class", "container");
            var pagediv = document.getElementById("widget-container");
            pagediv.appendChild(container);
            newWidget = new Cwidget(tempName, tempInfo, container);
        });
    });

}

//Widget Constructor
function Cwidget(name, info, container){
    this.name = name;
    this.info = info;

    var _container = container;
    _container.classList.add("widget-item");
    //Creates show graph button
    var _showGraph = document.createElement("button");
    _showGraph.textContent = "Show Graph";
    _showGraph.onclick = () =>{
        makeChart(name);
    }
    //Creates add to graph button
    var _addGraph = document.createElement("button");
    _addGraph.textContent = "Add To Graph";
    _addGraph.onclick = () =>{
        if(myChart){
        addToChart(name);
        }

    }
    //Creates delete button, removes widget from ui and dataSetArray.
    var _remove = document.createElement("button");
    _remove.textContent = "Delete";
    _remove.onclick = () =>{
        dashboard = document.getElementById("widget-container");
        dashboard.removeChild(_container);

        let index = widgets.indexOf(this.commodity);
        widgets.splice(index,1);
        
        let index2 = dataSetArray.indexOf(this.commodity);
        dataSetArray.splice(index,1);

        console.log(dataSetArray.length);
    };

    //Displays information and buttons on widget.
    var _nameLabel = document.createElement("h1");
    _nameLabel.textContent = name;

    var _infoLabel = document.createElement("p");
    _infoLabel.textContent = info;

    _container.appendChild(_nameLabel);
    _container.appendChild(_infoLabel);
    _container.appendChild(_showGraph);
    _container.appendChild(_addGraph);
    _container.appendChild(_remove);

}

//Contructor for data set objects.
class DataSet{
    constructor(code, labels, data){
        this.code = code;
        this.labels = labels;
        this.data = data;
        this.bWidth = 2;
        this.fill = false;
        this.active = false;
    }
    //See if data set is active or not.
    getActive() {
        return this.active;
    }
    //Set what dataset is being used for graph
    setActive(active){
        if(active == "true"){
            this.active = true;
        }
        else{
            this.active = false;
        }
    }



}

//Makes chart using chart.js.
function makeChart(name){
    let c = commodities.find(commodity => commodity.name === name);
    var code  = c.code;
    let d = dataSetArray.find(dataSet => dataSet.code ===code);

    //Sets to active dataset.
    d.setActive("true");
    active += 1;

    //Deletes mychart form canvas if it exists already.
    if(myChart){
        myChart.destroy();
    }

    //Passes in data being used for graph
    const dataGraph = {
        labels: d.labels,
        datasets: [
            {
            label: d.code,
            data: d.data,
            borderColor: "rgb(255, 99, 132)",
            borderWidth: d.bWidth,
            fill: d.fill
        },
    ],
    };

    const config = {
        type: "line",
        data: dataGraph,
    };
    //Constructs graph.
    myChart = new Chart(
        document.getElementById('myCanvas'),
        config,
    );
}

//Adds chart to canvas when there is an existing chart
function addToChart(name){
    
    if(myChart){
        myChart.destroy();
    }
    if(chartMulti){
        chartMulti.destroy();
    }

    let c = commodities.find(commodity => commodity.name === name);
    var code  = c.code;
    let dataSet1 = dataSetArray.find(dataSet => dataSet.code ===code);

    var dataSet2;

    //Gets already active chart from dataSetArray.
    for(var i = 0; i < dataSetArray.length; i++){
        let d = dataSetArray[i];
        if(d.getActive() == true){
            dataSet2 = d;
            break;
        }
    }
    

    if(dataSet2 == null){
        makeChart(name);
    }
    else{
    const ctx = document.getElementById("myCanvas").getContext("2d");
    chartMulti = new Chart(ctx, {
        type: "line",
        data: {
            labels: dataSet1.labels,
            datasets: [{
                label: dataSet1.code,
                data: dataSet1.data,
                borderColor: "rgb(255, 99, 132)",
                borderWidth: dataSet1.bWidth,
                fill: dataSet1.fill
            }, {
                label: dataSet2.code,
                data: dataSet2.data,
                borderColor: "rgb(0, 99, 132)",
                borderWidth: dataSet2.bWidth,
                fill: dataSet2.fill
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }

            }


        }
    });
    }

}

//Clears Graphs from canvas.
function clearGraph(){
    if(myChart || chartMulti){
    myChart.destroy();
    chartMulti.destroy();
    deactivate();
    }
}

//Sets all active datasets to false.
function deactivate(){
    for(var i = 0; i < dataSetArray.length; i++){
        let d = dataSetArray[i];
        d.setActive("false");
    }
}

//Sorts commodities array.
function cSort(a,b){
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();

    if(nameA < nameB){
        return -1;
    }
    if(nameA > nameB){
        return 1;
    }
    return 0;

}